<?php

namespace Drupal\paragraphs_simple_selection\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Plugin\EntityReferenceSelection\ParagraphSelection;

/**
 * Default plugin implementation of the Entity Reference Selection plugin.
 *
 * @EntityReferenceSelection(
 *   id = "paragraphs_simple_selection",
 *   label = @Translation("Paragraphs Simple Selection"),
 *   group = "paragraphs_simple_selection",
 *   entity_types = {"paragraph"},
 *   weight = -10
 * )
 */
class SimpleParagraphSelection extends ParagraphSelection {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return DefaultSelection::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return DefaultSelection::buildConfigurationForm($form, $form_state);
  }

  /**
   * Returns the sorted allowed types for the field.
   *
   * @return array
   *   A list of arrays keyed by the paragraph type machine name
   *   with the following properties.
   *     - label: The label of the paragraph type.
   *     - weight: The weight of the paragraph type.
   */
  public function getSortedAllowedTypes() {
    $return_bundles = [];

    $bundles = $this->entityTypeBundleInfo->getBundleInfo('paragraph');
    if (is_array($this->configuration['target_bundles'])) {
      $bundles = array_intersect_key($bundles, $this->configuration['target_bundles']);
    }

    array_multisort(array_keys($bundles), SORT_NATURAL | SORT_FLAG_CASE, $bundles);

    $weight = 0;

    foreach ($bundles as $machine_name => $bundle) {
      $return_bundles[$machine_name] = [
        'label' => $bundle['label'],
        'weight' => $weight,
      ];

      $weight++;
    }

    return $return_bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function validateReferenceableEntities(array $ids) {
    return DefaultSelection::validateReferenceableEntities($ids);
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    return DefaultSelection::buildEntityQuery($match, $match_operator);
  }

}
